#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include "test.h"

int test_new(test_t* test, char const* name, test_f body, unsigned int flags)
{
  assert(test);
  assert(name);
  size_t sz = (strlen(name) + 1) * sizeof(char);
  test->name = (char*) malloc(sz);
  strncpy(test->name, name, sz);
  test->name[sz-1] = '\0';
  test->body = body;
  test->flags = flags;
  
  return 0;
}

int test_delete(test_t* test)
{
  assert(test);

  free(test->name);

  return 0;
}
