#ifndef TEST_H
#define TEST_H

typedef int (*test_f)(void);

enum { AR_TEST_NO_FLAG = 0 };

typedef struct
{
  char* name;
  test_f body;
  unsigned int flags;
} test_t;

/* Create a test.
 * 
 * @param test_t* test, 
 * @param char const* name, the test name.
 * @param test_f body, the function body.
 * @param unsigned int flag, behavior of the test.
 * @return int, 0 on success.
 */
int test_new(test_t* test, char const* name, test_f body, unsigned int flags);

/* Delete a test.
 * @return int, 0 on success.
 */
int test_delete(test_t* test);

#endif
