#ifndef RUNNER_H
#define RUNNER_H

/* Basic test runner.
 * Run the tests sequentially.
 */
void runner_basic(void* data);


#endif
