#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "artest.h"

char const* artest_version()
{
  return AR_VERSION;
}

int artest_new(artest_t* artest, runner_f runner)
{
  assert(artest);
  assert(runner);
  
  artest->test_count = 0;
  artest->capacity = 1;
  artest->tests = malloc(artest->capacity * sizeof(test_t*));
  artest->runner = runner;
  
  return 0;
}

int artest_delete(artest_t* artest)
{
  assert(artest);

  // Delete projects.
  for (size_t i=0; i<artest->test_count; i++)
    {
      test_delete(artest->tests[i]);
      free(artest->tests[i]);
    }
  
  free(artest->tests);
  artest->tests = NULL;
  
  return 0;
}

void artest_add_test(artest_t* artest, char const* name, test_f body, unsigned int flags)
{
  assert(artest);

  test_t* new_test = (test_t*) malloc(sizeof(test_t));
  test_new(new_test, name, body, flags);

  if (artest->test_count + 1 >= artest->capacity)
    {
      artest->capacity *= 2;
      artest->tests = realloc(artest->tests, artest->capacity * sizeof(test_t));
    }

  artest->tests[artest->test_count++] = new_test;
}

void artest_print_test_names(artest_t* artest, FILE* stream)
{
  assert(artest);
  assert(stream);

  for (size_t i=0; i<artest->test_count; i++)
    {
      char* name = artest->tests[i]->name;
      size_t sz = strlen(name);
      fwrite(name, sizeof(char), sz, stream);

      char const* endline = "\n";
      fwrite(endline, sizeof(char), 1, stream);
    }
}

void artest_run(artest_t* artest)
{
  assert(artest);

  artest->runner(artest);
}
