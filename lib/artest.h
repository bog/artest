#ifndef ARTEST_H
#define ARTEST_H
#include <stdio.h>
#define AR_VERSION "v0.0"
#define pass() return 0
#define fail() return -1
#define fail_with_args(X) fprintf(stderr, "Test failed with : %s\n", #X); return -1
#define require(X) if((X) == (0)) { fail_with_args(X); }
#define test(X) int X ()
#define ar_begin(RUNNER) { artest_t artest; artest_new(&artest, RUNNER);
#define ar_end() artest_delete(&artest); }
#define ar_test(NAME, CALLBACK) artest_add_test(&artest, NAME, CALLBACK, 0);
#define ar_run() artest_run(&artest);

#include "test.h"
#include "runner.h"

typedef void (*runner_f)(void*);

typedef struct
{
  test_t** tests;
  size_t test_count;
  size_t capacity;

  runner_f runner;
} artest_t;

/* Print the version of ArTest.
 * @return char const*, the current version.
 */
char const* artest_version();

/* Create a new artest.
 * @return 0 on success.
 */
int artest_new(artest_t* artest, runner_f runner);

/* Delete a artest.
 * @return 0 on success.
 */
int artest_delete(artest_t* artest);

/*
 * @see test_new
 */
void artest_add_test(artest_t* artest, char const* name, test_f body, unsigned int flags);

/* 
 * Print the name of all the projects of artest.
 * @param FILE* stream, the output stream.
 */
void artest_print_test_names(artest_t* artest, FILE* stream);

/* 
 * Run the test execution.
 */
void artest_run(artest_t* artest);

#endif
