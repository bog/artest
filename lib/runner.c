#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include "runner.h"
#include "artest.h"

void runner_basic(void* data)
{
  assert(data);
  artest_t* artest = (artest_t*) data;
  
  char const* red_color = "\e[31;31m";
  char const* green_color = "\e[32;32m";
  char const* no_color = "\e[0;0m";
  unsigned int n_pass = 0;
  unsigned int n_failed = 0;
  
  for (size_t i = 0; i < artest->test_count; i++)
    {
      if ((*artest->tests[i]->body)() < 0)
	{
	  fprintf(stderr, "%s[FAILED] - %s%s\n", red_color, artest->tests[i]->name, no_color);
	  n_failed++;
	}
      else
	{
	  n_pass++;
	}
    }

  printf("\n\n---------------- REPORT ----------------\n");
  printf("%d test(s) : %s%d pass%s, %s%d failed%s.\n",
	 n_pass + n_failed,
	 green_color,
	 n_pass,
	 no_color,
	 red_color,
	 n_failed,
	 no_color);
}
