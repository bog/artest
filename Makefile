all: create_dirs build/bin/../lib/libartest.a build/bin/artest.elf 

create_dirs:
	@mkdir -p build/bin/../lib
	@mkdir -p build/bin
	@mkdir -p build/obj
	@mkdir -p build/obj/ArTestLib/a/lib
	@mkdir -p build/obj/ArTestLib/a/lib
	@mkdir -p build/obj/ArTestLib/a/lib
	@mkdir -p /usr/local/bin
	@mkdir -p /usr/local/lib
	@mkdir -p /usr/local/include
	@mkdir -p build/bin/../lib
	@mkdir -p build/bin
	@mkdir -p build/obj
	@mkdir -p build/obj/ArTest/elf/src
### ArTestLib

build/obj/ArTestLib/a/lib/test.o: lib/test.c lib/test.h
	gcc -c $< -Wall -g -fpic -o $@

build/obj/ArTestLib/a/lib/artest.o: lib/artest.c lib/artest.h
	gcc -c $< -Wall -g -fpic -o $@

build/obj/ArTestLib/a/lib/runner.o: lib/runner.c lib/runner.h
	gcc -c $< -Wall -g -fpic -o $@

build/bin/../lib/libartest.a: build/obj/ArTestLib/a/lib/test.o build/obj/ArTestLib/a/lib/artest.o build/obj/ArTestLib/a/lib/runner.o 
	ar q $@ $^

### ArTest

## artest.elf
build/obj/ArTest/elf/src/artest.o: src/artest.c
	gcc -c $< -Wall -L build/lib -g  -o $@



build/bin/artest.elf: build/obj/ArTest/elf/src/artest.o 
	gcc $^ -Wall -L build/lib -g  -lartest -o $@


u: update
	
update: 
	make mrproper
	duckcpp > Makefile
	make
.PHONY: clean mrproper install uninstall create_dirs u update
clean: 
	rm -f build/obj/ArTestLib/a/lib/test.o
	rm -f build/obj/ArTestLib/a/lib/artest.o
	rm -f build/obj/ArTestLib/a/lib/runner.o
	rm -f build/obj/ArTest/elf/src/artest.o
mrproper: clean
	rm -f build/bin/../lib/libartest.a
	rm -f build/bin/artest.elf
install: build/bin/../lib/libartest.a 
	cp -Rv build/bin/../lib/libartest.a /usr/local/lib/
	cp -Rv lib/test.h /usr/local/include/
	cp -Rv lib/runner.h /usr/local/include/
	cp -Rv lib/artest.h /usr/local/include/
uninstall: 
	rm -v /usr/local/lib/libartest.a
	rm -v /usr/local/include/test.h
	rm -v /usr/local/include/runner.h
	rm -v /usr/local/include/artest.h

