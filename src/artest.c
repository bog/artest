#include <assert.h>
#include <stdio.h>
#include "../lib/artest.h"
#include "../lib/runner.h"

test(f0)
{
  require(0 == 0);
  pass();
}

test(g)
{
  require(0 == 1);
  pass();
}

int main(int argc, char** argv)
{
  ar_begin(runner_basic);
  
  ar_test("test0", f0);
  ar_test("test1", g);
  
  ar_run();
  ar_end();

  ar_begin(runner_basic);
  
  ar_test("test2", f0);
  ar_test("test3", g);
  
  ar_run();
  ar_end();
 
  return 0;
}
